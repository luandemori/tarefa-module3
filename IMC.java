import java.util.Scanner;

public class IMC{
  public static void main(String[] args) {
    Scanner i = new Scanner(System.in);
    System.out.println("Seu peso em KG: ");
    double peso = i.nextDouble();
    System.out.println("Sua altura em metros: ");
    double altura = i.nextDouble();
    double IMC = peso / (altura * altura);
    System.out.print("Seu Indice de Massa Corporal é:\n" + IMC + " kg/m2");
  
  }
}